# Spring Session JDBC

# Config
Comptes : 
- user/user
- admin/admin

# Démarrage
- Démarrer la base
```
docker-compose up -d
```

- Démarrer l'application
```
./mvnw spring-boot:run
```

# Références

https://www.baeldung.com/spring-session-jdbc

https://www.baeldung.com/spring-security-jdbc-authentication

https://www.baeldung.com/spring-security-authentication-with-a-database

https://github.com/eugenp/tutorials/tree/master/spring-security-modules/spring-security-web-boot-2

https://howtodoinjava.com/java/serialization/custom-serialization-readobject-writeobject/

http://blog.paumard.org/cours/java/chap10-entrees-sorties-serialization.html

https://www.baeldung.com/java-serialization

https://www.jmdoudoux.fr/java/dej/chap-serialisation.htm