DROP TYPE IF EXISTS COUNTRY;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS authorities;

CREATE TYPE COUNTRY AS ENUM ('FRANCE', 'USA');

CREATE TABLE users
(
    username varchar(50)  NOT NULL PRIMARY KEY,
    password varchar(100) NOT NULL,
    enabled  boolean      NOT NULL DEFAULT true,
    country  COUNTRY      NOT NULL,
    company  varchar(50)  NULL,
    ssn      varchar(50)  NOT NULL,
    childs   INT          NOT NULL
);

CREATE TABLE authorities
(
    username  varchar(50) NOT NULL,
    authority varchar(50) NOT NULL,
    CONSTRAINT foreign_authorities_users_1 foreign key (username) references users (username)
);

CREATE UNIQUE INDEX ix_auth_username on authorities (username, authority);