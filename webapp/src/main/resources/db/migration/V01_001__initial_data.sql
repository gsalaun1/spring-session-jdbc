insert into users(username, password, enabled, country, company, ssn, childs)
values ('admin', '$2y$10$mPvFPpBQMJBj5aJ/XCtmtuGJ1SChdNJ98snmniwizB6WyW.6v5Etm', true, 'FRANCE', 'MAXDS', 'SSN ADMIN',
        2);
insert into authorities(username, authority)
values ('admin', 'ROLE_ADMIN');

insert into users(username, password, enabled, country, company, ssn, childs)
values ('user', '$2y$10$HqRnGoHW4Jq9yplmgP5FGO9Y3rqN2Krq1Y5x0cEu.dhGHZsi.mZK.', true, 'USA', 'DM', 'SSN USER', 3);
insert into authorities(username, authority)
values ('user', 'ROLE_USER');