package gsalaun1.springsessionjdbc.webapp.model

enum class Country(val code: String) {
    FRANCE("FR"), USA("US");

    companion object {
        fun findByCode(searchedCode: String) = values().firstOrNull { it.code == searchedCode }
    }
}
