package gsalaun1.springsessionjdbc.webapp.model

import gsalaun1.springsessionjdbc.domain.Company
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.io.ObjectInputStream
import java.io.Serializable

data class CustomUserDetails(
    private var username: String,
    private var password: String,
    var countryCode: String,
    var companyName: String? = null,
    var infos: UserInfos
) : UserDetails, Serializable {

    companion object {
        private const val serialVersionUID = -2089740685L
    }

    data class UserInfos(
        var ssn: String,
        var childs: Int
    ) : Serializable {

        companion object {
            private const val serialVersionUID = -128L
        }

        private fun readObject(ois: ObjectInputStream) {
            val fields = ois.readFields()
            ssn = fields.get("ssn", "") as String
            childs = fields.get("childs", 0)
        }
    }

    val country: Country?
        get() = Country.findByCode(countryCode)

    val company: Company?
        get() = companyName?.let { Company(it) }

    override fun getAuthorities() = emptyList<GrantedAuthority>()

    override fun getUsername() = username

    override fun getPassword() = password

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true

    override fun isCredentialsNonExpired() = true

    override fun isEnabled() = true

    private fun readObject(ois: ObjectInputStream) {
        val fields = ois.readFields()
        username = fields.get("username", null) as String
        password = fields.get("password", null) as String
        countryCode = fields.get("countryCode", null) as String
        companyName = fields.get("companyName", null) as String?
        infos = fields.get("infos", null) as UserInfos
    }
}
