package gsalaun1.springsessionjdbc.webapp.service

import gsalaun1.springsessionjdbc.webapp.model.CustomUserDetails
import gsalaun1.springsessionjdbc.webapp.repository.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class CustomUserDetailsService(
    val userRepository: UserRepository
) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByUsername(username) ?: throw UsernameNotFoundException(username)
        if (user.company == null) {
            return CustomUserDetails(
                user.username,
                user.password,
                user.country.code,
                null,
                CustomUserDetails.UserInfos(user.ssn, user.childs)
            )
        }
        return CustomUserDetails(
            user.username,
            user.password,
            user.country.code,
            user.company,
            CustomUserDetails.UserInfos(user.ssn, user.childs)
        )
    }
}
