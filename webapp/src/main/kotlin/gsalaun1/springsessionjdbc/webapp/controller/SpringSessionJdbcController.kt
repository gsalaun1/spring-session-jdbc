package gsalaun1.springsessionjdbc.webapp.controller

import gsalaun1.springsessionjdbc.webapp.security.IAuthenticationFacade
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession

@Controller
class SpringSessionJdbcController(
    val authenticationFacade: IAuthenticationFacade
) {
    @GetMapping("/")
    fun home(model: Model, httpSession: HttpSession): String {
        var messages = httpSession.getAttribute("SPRING_BOOT_SESSION_MESSAGES") as List<String>?
        if (messages == null) {
            messages = emptyList()
        }
        model.addAttribute("sessionMessages", messages)
        val userDetails = authenticationFacade.getUserDetails()
        model.addAttribute("username", userDetails.username)
        model.addAttribute("country", userDetails.country)
        model.addAttribute("company", userDetails.company?.name)
        model.addAttribute("ssn", userDetails.infos.ssn)
        model.addAttribute("childs", userDetails.infos.childs)

        return "index"
    }

    @PostMapping("/saveMessage")
    fun saveMessage(@RequestParam("msg") message: String, httpServletRequest: HttpServletRequest): String {
        var messages = httpServletRequest.session.getAttribute("SPRING_BOOT_SESSION_MESSAGES") as List<String>?
        if (messages == null) {
            messages = emptyList()
        }
        httpServletRequest.session.setAttribute("SPRING_BOOT_SESSION_MESSAGES", messages + message)
        return "redirect:/"
    }

    @PostMapping("/delete")
    fun deleteSession(httpServletRequest: HttpServletRequest): String? {
        httpServletRequest.session.invalidate()
        return "redirect:/"
    }
}
