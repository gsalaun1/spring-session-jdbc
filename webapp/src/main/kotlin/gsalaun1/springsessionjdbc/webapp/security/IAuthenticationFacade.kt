package gsalaun1.springsessionjdbc.webapp.security

import gsalaun1.springsessionjdbc.webapp.model.CustomUserDetails

interface IAuthenticationFacade {
    fun getUserDetails(): CustomUserDetails
}
