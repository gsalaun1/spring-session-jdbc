package gsalaun1.springsessionjdbc.webapp.security

import gsalaun1.springsessionjdbc.webapp.model.CustomUserDetails
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

@Component
class AuthenticationFacade : IAuthenticationFacade {
    override fun getUserDetails(): CustomUserDetails {
        return SecurityContextHolder.getContext().authentication.principal as CustomUserDetails
    }
}
