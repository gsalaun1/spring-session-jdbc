package gsalaun1.springsessionjdbc.webapp.entity

import gsalaun1.springsessionjdbc.webapp.model.Country
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "users")
data class DbUser(
    @Id val username: String = "",
    val password: String = "",
    @Enumerated(EnumType.STRING)
    val country: Country = Country.FRANCE,
    val company: String? = null,
    val ssn: String = "",
    val childs: Int = 0
)
