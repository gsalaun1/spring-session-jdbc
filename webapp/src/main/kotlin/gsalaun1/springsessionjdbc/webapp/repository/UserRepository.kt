package gsalaun1.springsessionjdbc.webapp.repository

import gsalaun1.springsessionjdbc.webapp.entity.DbUser
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<DbUser, String> {
    fun findByUsername(username: String): DbUser?
}
