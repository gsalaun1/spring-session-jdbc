package gsalaun1.springsessionjdbc.webapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringSessionJdbcApplication

fun main(args: Array<String>) {
    runApplication<SpringSessionJdbcApplication>(*args)
}
