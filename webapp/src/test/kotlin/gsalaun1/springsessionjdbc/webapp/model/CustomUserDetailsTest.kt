package gsalaun1.springsessionjdbc.webapp.model

import assertk.assertAll
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNull
import assertk.assertions.isSuccess
import gsalaun1.springsessionjdbc.domain.Company
import org.junit.jupiter.api.Test
import org.springframework.security.crypto.codec.Hex
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

class CustomUserDetailsTest {

    companion object {
        const val USERNAME = "username"
        const val PASSWORD = "password"
        val COUNTRY = Country.USA
        val COMPANY = Company("Company name")
        val SSN = "social security number"
        val CHILDS = 2

        const val SERIALIZED_WITH_COMPANY_V1 =
            "1f8b08000000000000009dd0b14ec3301006e0c328a8124c0c9d3b30c762448c81a152c452f5012e891b5c39b6eb3b53ba20245e8" +
                "757e3198a931465e8c64db66cfdffa7fbfe818c023cb48406a3bdcfc9076d5b5244dad96d53d5f95e55e87ddeb94699bc88c4" +
                "ae5b930a4f8a511b3aa6f9dacd494056c275ed3a8ff6f0829d62b82db7f886d2a06de58afbd8c7e14bb41c0e458adbc1075c9" +
                "49069bb71c4f05cfe29e499428e0a3928e499e2ae3f2ffb985431f348b477a139e5cf627ab48934dcdf3dc34d313a1776808a" +
                "f52aeda0f8f70ea6f6e3389f02c412aeea576d1a2ae192c89eba014030ccc9d51acd82541d83e6e4885da5024f749ed4bfa32" +
                "476d6a3010000"

        const val SERIALIZED_WITHOUT_COMPANY_V1 =
            "1f8b08000000000000009d8f3d4ec340108507474691e852a44e41ed1525a23414912c9a280718db8bd968ffb2334b708390b80e57" +
                "e30c61ed04b948c7ab6634a3f7bef7fd033905b8ef0835467b57900fca76248994b3bbb66e8a83acd1fbc2b856eaa28cc4ce6c" +
                "498647c9a8341d93bef64bca20afe0a671c6a3ed9fd1488645b5c337141a6d27363cd83e8c2fd172e8cb64b7870fb8aa2057f6" +
                "c511c353f547212e28c489428c14e282e27698d7834d8a987b243ab8d09efde7311d6d421af777ef19b2ed26b52effdd7aca3b" +
                "9ef49941b686ebe655e9962a9811d9731a00640c4b728d42bd22d9c4a0b85fd9686a197882e589f317485fa4ac95010000"
    }

    @Test
    fun `Should serialize and deserialize Custom User Details with Company`() {
        val customUserDetails = buildUserDetailsWithCompany()
        val serialized = serialize(customUserDetails)
        val unserialized = deserialize(serialized)
        assertThat(unserialized).isEqualTo(customUserDetails)
    }

    @Test
    fun `Should serialize and deserialize Custom User Details without Company`() {
        val customUserDetails = buildUserDetailsWithoutCompany()
        val serialized = serialize(customUserDetails)
        val unserialized = deserialize(serialized)
        assertThat(unserialized).isEqualTo(customUserDetails)
    }

    @Test
    fun `Should not throw any exception at deserialization of Custom User Details with Company`() {
        assertThat { deserialize(SERIALIZED_WITH_COMPANY_V1) }.isSuccess()
    }

    @Test
    fun `Should not throw any exception at deserialization of Custom User Details without Company`() {
        assertThat { deserialize(SERIALIZED_WITHOUT_COMPANY_V1) }.isSuccess()
    }

    @Test
    fun `Should deserialize Custom User Details with Company`() {
        val deserialized = deserialize(SERIALIZED_WITH_COMPANY_V1)
        assertAll {
            assertThat(deserialized.username).isEqualTo(USERNAME)
            assertThat(deserialized.password).isEqualTo(PASSWORD)
            assertThat(deserialized.country).isEqualTo(COUNTRY)
            assertThat(deserialized.company).isEqualTo(COMPANY)
        }
    }

    @Test
    fun `Should deserialize Custom User Details without Company`() {
        val deserialized = deserialize(SERIALIZED_WITHOUT_COMPANY_V1)
        assertAll {
            assertThat(deserialized.username).isEqualTo(USERNAME)
            assertThat(deserialized.password).isEqualTo(PASSWORD)
            assertThat(deserialized.country).isEqualTo(COUNTRY)
            assertThat(deserialized.company).isNull()
        }
    }

    private fun buildUserDetailsWithCompany() =
        CustomUserDetails(USERNAME, PASSWORD, COUNTRY.code, COMPANY.name, CustomUserDetails.UserInfos(SSN, CHILDS))

    private fun buildUserDetailsWithoutCompany() =
        CustomUserDetails(USERNAME, PASSWORD, COUNTRY.code, null, CustomUserDetails.UserInfos(SSN, CHILDS))

    private fun serialize(details: CustomUserDetails): String {
        val output = ByteArrayOutputStream()
        ObjectOutputStream(GZIPOutputStream(output)).use {
            it.writeObject(details)
        }
        return String(Hex.encode(output.toByteArray()))
    }

    private fun deserialize(gzippedHexData: String): CustomUserDetails {
        val bytes = Hex.decode(gzippedHexData)
        return ObjectInputStream(GZIPInputStream(ByteArrayInputStream(bytes))).use {
            it.readObject() as CustomUserDetails
        }
    }
}
