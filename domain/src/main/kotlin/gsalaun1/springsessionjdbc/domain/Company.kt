package gsalaun1.springsessionjdbc.domain

import java.io.Serializable

data class Company(val name: String) : Serializable
